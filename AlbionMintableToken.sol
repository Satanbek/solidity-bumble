pragma solidity ^0.4.18;

/**
 * @title ERC20 interface
 * @dev see https://github.com/ethereum/EIPs/issues/20
 */
contract ERC20 {
  function totalSupply() public view returns (uint256);

  function balanceOf(address _who) public view returns (uint256);

  function allowance(address _owner, address _spender) public view returns (uint256);

  function transfer(address _to, uint256 _value) public returns (bool);

  function approve(address _spender, uint256 _value) public returns (bool);

  function transferFrom(address _from, address _to, uint256 _value) public returns (bool);

  event Transfer(
    address indexed from,
    address indexed to,
    uint256 value
  );

  event Approval(
    address indexed owner,
    address indexed spender,
    uint256 value
  );
}

/**
 * @title SafeMath
 * @dev Math operations with safety checks that revert on error
 */
library SafeMath {

  /**
  * @dev Multiplies two numbers, reverts on overflow.
  */
  function mul(uint256 _a, uint256 _b) internal pure returns (uint256) {
    // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
    // benefit is lost if 'b' is also tested.
    // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
    if (_a == 0) {
      return 0;
    }

    uint256 c = _a * _b;
    require(c / _a == _b);

    return c;
  }

  /**
  * @dev Integer division of two numbers truncating the quotient, reverts on division by zero.
  */
  function div(uint256 _a, uint256 _b) internal pure returns (uint256) {
    require(_b > 0); // Solidity only automatically asserts when dividing by 0
    uint256 c = _a / _b;
    // assert(_a == _b * c + _a % _b); // There is no case in which this doesn't hold

    return c;
  }

  /**
  * @dev Subtracts two numbers, reverts on overflow (i.e. if subtrahend is greater than minuend).
  */
  function sub(uint256 _a, uint256 _b) internal pure returns (uint256) {
    require(_b <= _a);
    uint256 c = _a - _b;

    return c;
  }

  /**
  * @dev Adds two numbers, reverts on overflow. 
  */
  function add(uint256 _a, uint256 _b) internal pure returns (uint256) {
    uint256 c = _a + _b;
    require(c >= _a);

    return c;
  }
  
  /**
  * @dev Subtracts a and b to find difference 
  * then divides difference to 1 days to find current day.
  */
  function getCurrentDay(uint256 startDate, uint256 currentDate) internal pure returns (uint256) {
    uint256 diff = sub( currentDate, startDate );
    
    return div( diff, 1 days );
  }
  
}


/**
 * @title Standard ERC20 token
 *
 * @dev Implementation of the basic standard token.
 * https://github.com/ethereum/EIPs/issues/20
 * Based on code by FirstBlood: https://github.com/Firstbloodio/token/blob/master/smart_contract/FirstBloodToken.sol
 */
contract StandardToken is ERC20 {
  using SafeMath for uint256;

  mapping(address => uint256) balances;

  mapping (address => mapping (address => uint256)) internal allowed;

  uint256 totalSupply_;

  /**
  * @dev Total number of tokens in existence
  */
  function totalSupply() public view returns (uint256) {
    return totalSupply_;
  }

  /**
  * @dev Gets the balance of the specified address.
  * @param _owner The address to query the the balance of.
  * @return An uint256 representing the amount owned by the passed address.
  */
  function balanceOf(address _owner) public view returns (uint256) {
    return balances[_owner];
  }

  /**
   * @dev Function to check the amount of tokens that an owner allowed to a spender.
   * @param _owner address The address which owns the funds.
   * @param _spender address The address which will spend the funds.
   * @return A uint256 specifying the amount of tokens still available for the spender.
   */
  function allowance(
    address _owner,
    address _spender
   )
    public
    view
    returns (uint256)
  {
    return allowed[_owner][_spender];
  }

  /**
  * @dev Transfer token for a specified address
  * @param _to The address to transfer to.
  * @param _value The amount to be transferred.
  */
  function transfer(address _to, uint256 _value) public returns (bool) {
    require(_value <= balances[msg.sender]);
    require(_to != address(0));

    balances[msg.sender] = balances[msg.sender].sub(_value);
    balances[_to] = balances[_to].add(_value);
    emit Transfer(msg.sender, _to, _value);
    return true;
  }

  /**
   * @dev Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.
   * Beware that changing an allowance with this method brings the risk that someone may use both the old
   * and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this
   * race condition is to first reduce the spender's allowance to 0 and set the desired value afterwards:
   * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
   * @param _spender The address which will spend the funds.
   * @param _value The amount of tokens to be spent.
   */
  function approve(address _spender, uint256 _value) public returns (bool) {
    allowed[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
    return true;
  }

  /**
   * @dev Transfer tokens from one address to another
   * @param _from address The address which you want to send tokens from
   * @param _to address The address which you want to transfer to
   * @param _value uint256 the amount of tokens to be transferred
   */
  function transferFrom(
    address _from,
    address _to,
    uint256 _value
  )
    public
    returns (bool)
  {
    require(_value <= balances[_from]);
    require(_value <= allowed[_from][msg.sender]);
    require(_to != address(0));

    balances[_from] = balances[_from].sub(_value);
    balances[_to] = balances[_to].add(_value);
    allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
    emit Transfer(_from, _to, _value);
    return true;
  }

  /**
   * @dev Increase the amount of tokens that an owner allowed to a spender.
   * approve should be called when allowed[_spender] == 0. To increment
   * allowed value is better to use this function to avoid 2 calls (and wait until
   * the first transaction is mined)
   * From MonolithDAO Token.sol
   * @param _spender The address which will spend the funds.
   * @param _addedValue The amount of tokens to increase the allowance by.
   */
  function increaseApproval(
    address _spender,
    uint256 _addedValue
  )
    public
    returns (bool)
  {
    allowed[msg.sender][_spender] = (
      allowed[msg.sender][_spender].add(_addedValue));
    emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
    return true;
  }

  /**
   * @dev Decrease the amount of tokens that an owner allowed to a spender.
   * approve should be called when allowed[_spender] == 0. To decrement
   * allowed value is better to use this function to avoid 2 calls (and wait until
   * the first transaction is mined)
   * From MonolithDAO Token.sol
   * @param _spender The address which will spend the funds.
   * @param _subtractedValue The amount of tokens to decrease the allowance by.
   */
  function decreaseApproval(
    address _spender,
    uint256 _subtractedValue
  )
    public
    returns (bool)
  {
    uint256 oldValue = allowed[msg.sender][_spender];
    if (_subtractedValue >= oldValue) {
      allowed[msg.sender][_spender] = 0;
    } else {
      allowed[msg.sender][_spender] = oldValue.sub(_subtractedValue);
    }
    emit Approval(msg.sender, _spender, allowed[msg.sender][_spender]);
    return true;
  }

}

/**
 * @title Ownable
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */
contract Ownable {
  address public owner;


  event OwnershipRenounced(address indexed previousOwner);
  event OwnershipTransferred(
    address indexed previousOwner,
    address indexed newOwner
  );


  /**
   * @dev The Ownable constructor sets the original `owner` of the contract to the sender
   * account.
   */
  constructor() public {
    owner = msg.sender;
  }

  /**
   * @dev Throws if called by any account other than the owner.
   */
  modifier onlyOwner() {
    require(msg.sender == owner);
    _;
  }

  /**
   * @dev Allows the current owner to relinquish control of the contract.
   * @notice Renouncing to ownership will leave the contract without an owner.
   * It will not be possible to call the functions with the `onlyOwner`
   * modifier anymore.
   */
  function renounceOwnership() public onlyOwner {
    emit OwnershipRenounced(owner);
    owner = address(0);
  }

  /**
   * @dev Allows the current owner to transfer control of the contract to a newOwner.
   * @param _newOwner The address to transfer ownership to.
   */
  function transferOwnership(address _newOwner) public onlyOwner {
    _transferOwnership(_newOwner);
  }

  /**
   * @dev Transfers control of the contract to a newOwner.
   * @param _newOwner The address to transfer ownership to.
   */
  function _transferOwnership(address _newOwner) internal {
    require(_newOwner != address(0));
    emit OwnershipTransferred(owner, _newOwner);
    owner = _newOwner;
  }
}



/**
 * @title Mintable token
 * @dev Simple ERC20 Token example, with mintable token creation
 * Based on code by TokenMarketNet: https://github.com/TokenMarketNet/ico/blob/master/contracts/MintableToken.sol
 */
contract MintableToken is StandardToken, Ownable {
  event Mint(address indexed to, uint256 amount);
  event MintFinished();

  bool public mintingFinished = false;


  modifier canMint() {
    require(!mintingFinished);
    _;
  }

  modifier hasMintPermission() {
    require(msg.sender == owner);
    _;
  }
  
  /**
   * @dev Function to mint tokens
   * @param _to The address that will receive the minted tokens.
   * @param _amount The amount of tokens to mint.
   * @return A boolean that indicates if the operation was successful.
   */
  function mint(
    address _to,
    uint256 _amount
  )
    public
    hasMintPermission
    canMint
    returns (bool)
  {
    totalSupply_ = totalSupply_.add(_amount);
    balances[_to] = balances[_to].add(_amount);
    emit Mint(_to, _amount);
    emit Transfer(address(0), _to, _amount);
    return true;
  }

  /**
   * @dev Function to stop minting new tokens.
   * @return True if the operation was successful.
   */
  function finishMinting() public onlyOwner canMint returns (bool) {
    mintingFinished = true;
    emit MintFinished();
    return true;
  }
}


contract Albion is MintableToken {
    
    string public constant name = "Albion";
    
    string public constant symbol = "ION";
    
    uint32 public constant decimals = 18;

}


contract Crowdsale is Ownable {
    using SafeMath for uint;
    
    uint period; // Сколько дней выделено для ICO
    uint public startTime; // Дата старта ICO по секундам
    uint public endTime;
    address coldWallet; // Адрес куда зачисляются Ehter
    uint restrictedPrecent; // Сколько % наших токенов мы получим в конце ICO
    address restricted; // Адрес куда перечислим токены коне ICO
    uint public softcap;
    uint public hardcap; // Нужная сумма сбора в WEI (1 ether = 1000000000000000000 (18 нуля) wei)
    uint rate; // коэффициент пересчета эфира в наши токены, ourToken = 1 ether * 100 rate;
    uint public constant oneEtherInWei = 1 ether; // 18 нуля
    mapping(address=>uint) balances; // храним кто сколько Eth отпривил
        
    Albion public token = new Albion();
    
    constructor() public {
        period = 28;
        startTime = 1533121200;
        endTime = startTime.add( period.mul( 1 days ) );
        
        coldWallet = 0xCA35b7d915458EF540aDe6068dFe2F44E8fa733c;
        
        restrictedPrecent = 40;
        restricted = 0x14723A09ACff6D2A60DcdF7aA4AFf308FDDC160C;
        
        hardcap = oneEtherInWei.mul( 110 );
        softcap = oneEtherInWei.mul( 60 );
        
        rate = oneEtherInWei.mul( 100 );
    }
    
    function currenctDayOfICO() public constant returns (uint) {
        return startTime.getCurrentDay(now);
    }
    

    modifier saleIsOn() {
        require(now > startTime && now < startTime + period * 1 days);
        
        _;
    }
    
    modifier isIcoFinished() {
        require(now > startTime + period * 1 days);
        _;
    }
    
    modifier isUnderHardCap() {
        require(address(this).balance <= hardcap);
        
        _;
    }
    
    
    function getBonusForDay() public returns (uint precent) {
        uint daysFromIcoStart = startTime.getCurrentDay(now);
        
             if (daysFromIcoStart == 1)  return 90; // +90% tokens
        else if (daysFromIcoStart == 2)  return 85; // +85% tokens
        else if (daysFromIcoStart == 3)  return 80; // +80% tokens
        else if (daysFromIcoStart == 4)  return 75; // +75% tokens
        else if (daysFromIcoStart == 5)  return 70; // +70% tokens
        else if (daysFromIcoStart == 6)  return 65; // +65% tokens
        else if (daysFromIcoStart == 7)  return 60; // +60% tokens
        else if (daysFromIcoStart == 8)  return 55; // +55% tokens
        else if (daysFromIcoStart == 9)  return 50; // +50% tokens
        else if (daysFromIcoStart == 10) return 45; // +45% tokens
        else if (daysFromIcoStart == 11) return 40; // +40% tokens
        else if (daysFromIcoStart == 12) return 38; // +38% tokens
        else if (daysFromIcoStart == 13) return 36; // +36% tokens
        else if (daysFromIcoStart == 14) return 34; // +34% tokens
        else if (daysFromIcoStart == 15) return 32; // +32% tokens
        else if (daysFromIcoStart == 16) return 30; // +30% tokens
        else if (daysFromIcoStart == 17) return 28; // +28% tokens
        else if (daysFromIcoStart == 18) return 26; // +26% tokens
        else if (daysFromIcoStart == 19) return 24; // +24% tokens
        else if (daysFromIcoStart == 20) return 22; // +22% tokens
        else if (daysFromIcoStart == 21) return 20; // +20% tokens
        else if (daysFromIcoStart == 22) return 18; // +18% tokens
        else if (daysFromIcoStart == 23) return 16; // +16% tokens
        else if (daysFromIcoStart == 24) return 14; // +14% tokens
        else if (daysFromIcoStart == 25) return 12; // +12% tokens
        else if (daysFromIcoStart == 26) return 10; // +10% tokens
        else if (daysFromIcoStart == 27) return 8;  // +8%  tokens
        else if (daysFromIcoStart == 28) return 7;  // +7%  tokens
        
        else return 0; // +0% tokens
    }
    
    function getBonusForEther(uint etherAmount) private returns (uint) {
             if (etherAmount >= 100 ether) return 50; // +50% tokens
        else if (etherAmount >= 50  ether) return 40; // +40% tokens
        else if (etherAmount >= 30  ether) return 30; // +30% tokens
        else if (etherAmount >= 10  ether) return 20; // +20% tokens
        else if (etherAmount >= 5   ether) return 10; // +10% tokens
        else if (etherAmount >= 1   ether) return 5;  // +5%  tokens
        
        else return 0; // +0% tokens
    }
    
    function getBonusTokens(uint etherAmount, uint sendTokensAmount) private returns (uint) {
        uint bonusPrecentForEther = getBonusForEther(etherAmount);
        uint bonusPrecentForDay = getBonusForDay();
        
        /**
         * Colculate bonus tokens:
         * ex: suppose we give to user toekns= = 200 of our token, and we need to calculate bonus tokens.
         * 1) get for bonusPrecentForEther = 30%
         * 2) get for bonusPrecentForDay   = 20%
         * 
         * calculate bonus tokens formula is = tokens * bonusPrecent / 100%
         * 
         * bonusTokens = 200 * (30 + 20) / 100 = 100
         * 
         * totalTokens = tokens (200) + bonusTokens (100) = 300
        */
        uint bonusTokens = ( sendTokensAmount.mul( bonusPrecentForEther.add(bonusPrecentForDay) ) ).div( 100 );
        
        return bonusTokens;
    }   
    
    
    function createTokens() public saleIsOn isUnderHardCap payable {
        balances[msg.sender] += msg.value;
        
        uint tokens = rate.mul(msg.value).div(oneEtherInWei);
        
        uint bonusTokens = getBonusTokens(msg.value, tokens);
        
        token.mint(msg.sender, tokens.add(bonusTokens));
        
    }
    
    
    function refund() public isIcoFinished {
        require(softcap > address(this).balance);
        require(balances[msg.sender] > 0);
        
        msg.sender.transfer(balances[msg.sender]);
        balances[msg.sender] = 0;
    }
    
    function finishMinting() public onlyOwner {
        endTime = now;
        uint issuedTokenSupply = token.totalSupply();
        uint restrictedTokens = issuedTokenSupply.mul(restrictedPrecent).div( 100 );
        token.mint(restricted, restrictedTokens);
        token.finishMinting();
    }
    
    function withdrawEther() public onlyOwner {
        require(endTime < now);
        require(address(this).balance > 0);
        require(softcap <= address(this).balance);
        
        coldWallet.transfer(address(this).balance);
    }
    
    // fallback функция вызывается у контракта, когда на него пересылают эфир 
    function() external payable saleIsOn {
        createTokens();
    }
}